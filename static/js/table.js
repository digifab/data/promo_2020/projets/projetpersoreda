(async () => {

    // "await" permet au JS de terminer de charger la route avant de passer à 
    // la ligne suivante.
    // axios.get permet de recupérer une donnée.
    const json = await axios.get('/datatable');
    const jsonn = json.data;

    // Console.log permet de voir dans l'inspecteur de la page web si la donnée
    // est correctement récupérée.
    console.log(json)

    // Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

        google.charts.load('current', {'packages': ['table']});
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Nom');
            data.addColumn('number', 'Eau g');
            data.addColumn('number', 'Vitamine K µg');
            data.addColumn('number', 'Sodium mg');
            data.addColumn('number', 'Cuivre mg');
            data.addColumn('number', 'Protéines g');
            data.addColumn('number', 'Bêta-carotène µg');
            data.addColumn('number', 'Magnésium mg');
            data.addColumn('number', 'Phosphore mg');
            data.addColumn('number', 'Calories kcal');
            data.addColumn('number', 'Vitamine C mg');
            data.addColumn('number', 'Potassium mg');
            data.addColumn('number', 'Vitamine B2 mg');
            data.addColumn('number', 'Vitamine B1 mg');
            data.addColumn('number', 'Manganèse mg');
            data.addColumn('number', 'Zinc mg');
            data.addColumn('number', 'Vitamine E mg');
            data.addColumn('number', 'Vitamine A mg');
            data.addColumn('number', 'Fibres g');
            data.addColumn('number', 'Vitamine B9 µg');
            data.addColumn('number', 'Lipides g');
            data.addColumn('number', 'Vitamine B3 mg');
            data.addColumn('number', 'Vitamine B6 mg');
            data.addColumn('number', 'Calcium mg');
            data.addColumn('number', 'Glucides g');
            data.addColumn('number', 'Vitamine B5 mg');
            data.addColumn('number', 'Energie kJ');
            data.addColumn('number', 'Fer mg');
            data.addColumn('number', 'Fibres alimentaires g');
            data.addRows(jsonn[i].data);

            var table = new google.visualization.Table(document.getElementById(jsonn[i].table));

            table.draw(data, {
                showRowNumber: true,
                width: '100%',
                height: '100%'
            });

            var table = new google.visualization.ChartWrapper({
                chartType: 'Table',
                containerId: 'tableJS',
                options: {
                    cssClassNames: cssClassNames,
                    allowHtml: true
                }
            });

        }
    }
})()

// Ajout de la barre de recherche dans le tableau.
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("tableJS1");
    tr = table.getElementsByTagName("tr");

    // Parcours toutes les lignes de la table et masque celles qui ne
    // correspondent pas à la requête de recherchée.
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}