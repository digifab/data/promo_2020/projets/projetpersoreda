(async () => {

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
    const json = await axios.get('/datacourbe');
    const jsonn = json.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donnée est
// correctement récupérée.
    console.log(json.data)

// Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
        let newDiv = document.createElement('div');
        newDiv.id = jsonn[i].table;
        document.body.appendChild(newDiv);

// Créer le graphique
        var chart = am4core.create(jsonn[i].table, am4charts.XYChart);

        am4core.useTheme(am4themes_animated);

// Ajout de la donnée
        chart.data = jsonn[i].data;

// Création des axes
        var valuesAxis = chart.xAxes.push(new am4charts.ValueAxis());
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Création des séries
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "y";
        series.dataFields.valueX = "x";
        series.tooltipText = "{value}"
        series.strokeWidth = 2;
        series.minBulletDistance = 10;

// Illustre les points par des cercles
        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.circle.strokeWidth = 2;
        bullet.circle.radius = 4;
        bullet.circle.fill = am4core.color("#fff");

// Ajout d'un scrollbar
        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarX.parent = chart.leftAxesContainer;

        var bullethover = bullet.states.create("hover");
        bullethover.properties.scale = 1.3;

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.fullWidthLineX = true;
        chart.cursor.lineX.fill = am4core.color("#000");
        chart.cursor.lineX.fillOpacity = 0.1;

        valuesAxis.title.text = "Les x";
        valueAxis.title.text = "Les y";
    }
})()