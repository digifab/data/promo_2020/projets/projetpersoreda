(async () => {

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
    const json = await axios.get('/datadiagrammes_circulaires');
    const jsonn = json.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donnée est
// correctement récupérée.
    console.log(json)

// Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
        let newDiv = document.createElement('div');
        newDiv.id = jsonn[i].table;
        document.body.appendChild(newDiv);

// Le themes d'animation
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create(jsonn[i].table, am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0;

        chart.data = jsonn[i].data;

        chart.innerRadius = am4core.percent(40);
        chart.depth = 80;

// Caracteristiques des legendes
        chart.legend = new am4charts.Legend();
        chart.legend.position = "absolute";
        chart.legend.maxHeight = 200;
        chart.legend.scrollable = true;

// Titre du graphique
        var label = chart.createChild(am4core.Label);
        label.text = jsonn[i].label;
        label.fontSize = 50;
        label.align = "center";

        var pieSeries = chart.series.push(new am4charts.PieSeries3D());
        pieSeries.dataFields.value = "valeur_nutritionnelle";
        pieSeries.dataFields.depthValue = "valeur_nutritionnelle";
        pieSeries.dataFields.category = "aliment";
        pieSeries.ticks.template.disabled = true;
        pieSeries.labels.template.disabled = true;
        pieSeries.slices.template.cornerRadius = 5;
        pieSeries.colors.step = 3;
    }

})()