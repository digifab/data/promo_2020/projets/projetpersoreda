# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 15:54:38 2020

Scraping des tableaux sur les différents fruits et légumes du site
Passeportsante.

Une fois traitées, ces données seront placées dans une base de données Postgre
pour y être visualiser et utiliser sur Digidashboard.

Plan :

I - Faire le tutoriel de selenium

II - Scraper un premier tableau
    a - Le transformer en datafram
    b - Le transformer en excel

III - Boucler la méthode de scrap utilisé pour le premier tableau

IV - Créer une Dataframe
    a - Une première colonne avec le nom des fruits/légumes
    b - Trier les noms des autres colonnes (vitamines, minéraux etc)
    c - Ajouter les colonnes dans la dataframe
    d - Trier les valeurs des tableaux scrapés
    e - Remplir la Dataframe par les valeurs triées

V - Sortir la Dataframe complère
    a - En csv ou excel
    b - Dans une table sur postgre

VI - Visualiser les données de la table sur DIGIDASHBOARD

VII - Intéraction avec les données
    a - Fonctionnalité de carences
    b - Peut être d'autres fonctionnalitées


TODO : Ne pas oublier de scraper les fruits manquants sur :
   https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Index.aspx

@author: Reda
"""

import math
import time
# from pandas import ExcelWriter
from datetime import datetime

import numpy as np
import pandas as pd
import psycopg2
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from sqlalchemy import create_engine

# Utilisation de webdriver.exe pour permmetre à selenium de lancer Chrome
PATH = r"D:\Boscope\Documents\ProjetPersoReda\Scrapping\chromedriver.exe"
driver = webdriver.Chrome(PATH)

# Page de base pour commencer les actions de selenium
driver.get("https://www.passeportsante.net/les-fruits-et-legumes-g153")

# Navigation sur les pages web du site, avec les cliques et les backpage.
# La navigation se fait soir par l'xpath, soit par l'id, le time sleep permet
# de laisser le temps à la page de se charger correctement.
# Puis on transforme le tableau en dataframe et en excel.

# Liste des tableaux scrapés
tableaux_scrapes = []

# Liste des fruits et légumes
fruits_legumes = []

# Liste des valeurs nutritives
valeurs_nutritives = []

listealiments = [
    "Abricot", "Acérola", "Airelles", "Ananas", "Argousier",
    "Artichaut", "Asperge", "Aubergine", "Avocat", "Baie de goji",
    "Banane", "Bette à carde", "Betterave", "Brocoli", "Brugnon",
    "Canneberge", "Carambole", "Carotte", "Cassis", "Chou chinois",
    "Chou frisé", "Chou-rave", "Chou", "Choux de Bruxelles",
    "Citrouille", "Clémentine", "Coing", "Combava",
    "Concombre et Cornichon", "Courges", "Courgette", "Céleri",
    "Datte", "Endive", "Epinards", "Fenouil", "Figue", "Fraise",
    "Framboise", "Fruit de la passion", "Goyave", "Grenade",
    "Griottes", "Groseille", "Haricot vert", "Kaki", "Kiwi",
    "Kumquat", "Laitue", "Litchi", "Mangue", "Melon", "Mirabelle",
    "Myrtille", "Mâche", "Noix de coco", "Pamplemousse",
    "Pastèque", "Patate douce", "Poire", "Poireau", "Poivron",
    "Pomelo", "Pomme de terre", "Prune et pruneau", "Pêche",
    "Quetsche", "Radis", "Raisin", "Rhubarbe", "Roquette",
    "Tomate", "Topinambour", "Yuzu"
]

date_creation_fichier = datetime.now().strftime("%d-%m-%Y_%I-%M-%S")

# Le scraping se fait par ordre alphabétique (legumes et fruits commençant par
# la lettre "A", la lettre "B" etc).
# Certains éléments d'une lettre peuvent ne pas être un légume ou un fruit
# mais par exemple un conseil de nutrition, c'est pour cette raison qu'il peut
# y avoir plusieurs boucles pour une même lettre.

try:

    # Clique sur "accepter" dans une alerte de cookie
    element = WebDriverWait(
        driver, 10).until(
        (EC.presence_of_element_located(
            (By.XPATH, '//button[@class="didomi-components-button didomi-'
                       'button didomi-dismiss-button didomi-components-button-'
                       '-color didomi-button-highlight highlight-button"]'))))
    element.click()

    # =========================================================================
    # ----- Récupération des aliments commençant par          "A" -------------
    # =========================================================================

    x = 0
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_2_rptColumns_2_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))

        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        # tableau_scrape.to_excel(str(date_creation_fichier)+" "+str(x)+'.xlsx')
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    x = 0
    while x < 2:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_2_rptColumns_2_"
                        "_1_lnkDoc_" + str(x) + ""))))

        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    x = 3
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_2_""rptColumns_2_"
                        "rptDocuments_1_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "B" -------------
    # =========================================================================

    x = 0
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_3_rptColumns_3_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        x += 1

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_3_rptColumns_3_"
                    "rptDocuments_1_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "C" -------------
    # =========================================================================

    x = 0
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_4_rptColumns_4_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_4_rptColumns_4_"
                        "rptDocuments_1_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        x += 1

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_4_"
                    "rptColumns_4_rptDocuments_1_lnkDoc_6"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    x = 0
    while x < 3:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_4_rptColumns_4_"
                        "rptDocuments_2_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    x = 4
    while x < 7:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_4_rptColumns_4_"
                        "rptDocuments_2_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "D" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_5_"
                    "rptColumns_5_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "E" -------------
    # =========================================================================

    x = 0
    while x < 2:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_6_rptColumns_6_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "F" -------------
    # =========================================================================

    x = 1
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_7_rptColumns_7_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_7_"
                    "rptColumns_7_rptDocuments_1_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "G" -------------
    # =========================================================================

    x = 0
    while x < 4:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_8_rptColumns_8_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "H" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_9_"
                    "rptColumns_9_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "K" -------------
    # =========================================================================

    x = 0
    while x < 3:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_10_rptColumns_10_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "L" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_11_"
                    "rptColumns_11_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_11_"
                    "rptColumns_11_rptDocuments_0_lnkDoc_3"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "M" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_12_"
                    "rptColumns_12_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    x = 3
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_12_rptColumns_12_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    x = 1
    while x < 3:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_12_rptColumns_12_"
                        "rptDocuments_1_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "N" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_13_"
                    "rptColumns_13_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "P" -------------
    # =========================================================================
    x = 0
    while x < 4:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_14_rptColumns_14_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_14_rptColumns_14_"
                        "rptDocuments_1_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)
        x += 1

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_14_"
                    "rptColumns_14_rptDocuments_2_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_14_"
                    "rptColumns_14_rptDocuments_2_lnkDoc_2"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "Q" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_15_"
                    "rptColumns_15_rptDocuments_0_lnkDoc_1"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

    # =========================================================================
    # ----- Récupération des aliments commençant par          "R" -------------
    # =========================================================================

    x = 0
    while x < 2:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_16_rptColumns_16_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        x += 1

    x = 3
    while x < 5:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_16_rptColumns_16_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "T" -------------
    # =========================================================================

    x = 0
    while x < 2:
        element = WebDriverWait(
            driver, 10).until(
            (EC.element_to_be_clickable(
                (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                        "LetterIndexRepeater_LetterIndex_17_rptColumns_17_"
                        "rptDocuments_0_lnkDoc_" + str(x) + ""))))
        fruits_legumes.append(element.text)
        element.click()

        time.sleep(1)
        tableau_scrape = pd.read_html(driver.current_url)
        tableau_scrape = tableau_scrape[1]
        tableaux_scrapes.append(tableau_scrape)

        driver.back()
        time.sleep(1)

        x += 1

    # =========================================================================
    # ----- Récupération des aliments commençant par          "Y" -------------
    # =========================================================================

    element = WebDriverWait(
        driver, 10).until(
        (EC.element_to_be_clickable(
            (By.ID, "cph_ContenuFichePlaceHolder_LetterIndexes_"
                    "LetterIndexRepeater_LetterIndex_18_"
                    "rptColumns_18_rptDocuments_0_lnkDoc_0"))))
    fruits_legumes.append(element.text)
    element.click()

    time.sleep(1)
    tableau_scrape = pd.read_html(driver.current_url)
    tableau_scrape = tableau_scrape[1]
    tableaux_scrapes.append(tableau_scrape)

    driver.back()
    time.sleep(1)

finally:
    print("Terminé")

# ============================================================================
# ----------------------------------- TRIE ------------------------------------
# ============================================================================

# Ajout d'une colonne "Nom" avec pour valeur les noms de la liste
#  "fruits_legumes" dans une Dataframe, à l'aide d'un dictionnaire.
dico_colonne_nom = {"Nom": []}

for fruit_legume in fruits_legumes:
    dico_colonne_nom["Nom"].append(fruit_legume)

tableau_des_aliments = pd.DataFrame(dico_colonne_nom)
print(tableau_des_aliments)

# Itère sur les lignes de la première colonne de chaque dataframe dans la liste
# "tableaux_scrapes".
# Ajoute les noms de colonnes à la liste "valeurs_nutritives".
for i in tableaux_scrapes:
    for ligne in i.index:
        # print(ligne)
        print(i.loc[ligne, 0])
        valeurs_nutritives.append(i.loc[ligne, 0])

# Suppression des noms de colonnes en doublons.
valeurs_nutritives = list(set(valeurs_nutritives))

# Trie les noms d'une liste en fonction de leurs nombre de lettres.
# J'ignore la raison pour le moment, mais pour supprimer les valeurs
# nutritionelles correctement, la boucle for doit être lancer plusieurs fois...
x = 0
while x < 4:
    for valeur_nutritive in valeurs_nutritives:
        if len((str(valeur_nutritive))) > 19:
            valeurs_nutritives.remove(valeur_nutritive)
            x += 1

# Supression des colonnes inutiles restantes.
valeurs_nutritives.remove('Pouvoir\xa0antioxydant')
valeurs_nutritives.remove('Charge glycémique\xa0:')
valeurs_nutritives.remove('Pour 28 g ou 75 ml')

# Supprime la colonne NaN.
del valeurs_nutritives[0]
print(valeurs_nutritives)

# ============================================================================
# ------------------------ CONSTRUCTION DE LA DATAFRAME -----------------------
# ============================================================================


# Ajout des colonnes dans la Dataframe.
for valeur_nutritive in valeurs_nutritives:
    tableau_des_aliments[valeur_nutritive] = ''

# ============================================================================
# ------------------------ REMPLISSAGE DE LA DATAFRAME ----------------------
# ============================================================================

# Reindex les nom et fruit
tablealiments = tableau_des_aliments
tablealiments = tablealiments.reindex(tablealiments.Nom)
tablealiments = tablealiments.drop('Nom', axis=1)

dicoaliment = {}
# La liste des dataframes(aliment) dans un dictionnaire :
#   clé = nom de l'aliment et valeur = la dataframe de l'aliment
for dataframe, aliment in zip(tableaux_scrapes, listealiments):
    dicoaliment[aliment] = dataframe

# On remplis la dataframe principale par les valeurs des dataframes des
# aliments
for aliment, dataframe in dicoaliment.items():
    x = 0
    while x < len(dataframe):
        try:
            tablealiments.loc[aliment, dicoaliment[aliment].loc[x, 0]] = (
                dicoaliment[aliment].loc[x, 1])
        except BaseException:
            continue
        x += 1

# On supprime les colonnes inutiles, par le nombres de lettre, et par leurs
# noms directement (pour une raison que j'ignore la totalité n'est pas
# supprimée)
x = 0
while x < 4:
    for valeur_nutritive in tablealiments.columns:
        print(valeur_nutritive)
        if len((str(valeur_nutritive))) > 19:
            tablealiments = tablealiments.drop([valeur_nutritive], axis=1)
            x += 1

tablealiments = tablealiments.drop(['Pouvoir\xa0antioxydant'], axis=1)
tablealiments = tablealiments.drop(['Charge glycémique\xa0:'], axis=1)
tablealiments = tablealiments.drop(['Pour 28 g ou 75 ml'], axis=1)
tablealiments = tablealiments.drop([tablealiments.columns[34]], axis=1)
tablealiments = tablealiments.drop(['Charge glycémique'], axis=1)
tablealiments = tablealiments.drop(['Nutriments'], axis=1)
tablealiments = tablealiments.drop(['Pour 100 g'], axis=1)

# Création de la table.
engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')
tablealiments.to_sql('table_aliments2', engine, index=True)

# Rename des colonnes pour ajouter la mesure (gramme, mg, µg etc), pour 
# pouvoir avoir ensuite une dataframe de flottants

# Liste des colonnes ayant pour unitée de mesure :
mg = []
µg = []
g = []
kcal = []
kJ = []

# Itère sur les valeurs des colonnes pour savoir quelles unitées de mesures
# elles contiennent
for column in tablealiments.columns:
    untableau = tablealiments[column].values
    for valeur in untableau:
        print(valeur)
        if 'mg' in str(valeur):
            mg.append(column)
        elif 'µg' in str(valeur):
            µg.append(column)
        elif 'g' in str(valeur):
            g.append(column)
        elif 'kcal' in str(valeur):
            kcal.append(column)
        elif 'kJ' in str(valeur):
            kJ.append(column)
        else:
            print(False)

# Suppresion des doublons
mg = list(set(mg))
µg = list(set(µg))
g = list(set(g))
kcal = list(set(kcal))
kJ = list(set(kJ))

liste_doublon_colonne = []
# 4 colonnes sont identiques, TODO : traiter ça, on gardera la colonne
# "Bêta-carotène"
for column in tablealiments.columns:
    if 'carot' in str(column):
        print(column)
        liste_doublon_colonne.append(column)

# Ajouts des valeurs des colonnes en doublon à la colonne gardée
tablealiments['Bêta-carotènes'].dropna()
tablealiments.xs('Brugnon')['Bêta-carotène'] = '150 µg'
tablealiments.xs('Carotte')['Bêta-carotène'] = '25 µg'
tablealiments.xs('Chou frisé')['Bêta-carotène'] = '5930 µg'
tablealiments.xs('Myrtille')['Bêta-carotène'] = '200 µg'
tablealiments.xs('Pomme de terre')['Bêta-carotène'] = '552 µg'

tablealiments['Bêta carotènes'].dropna()
tablealiments.xs('Quetsche')['Bêta-carotène'] = '190 µg'

tablealiments['Bêta carotène'].dropna()
tablealiments.xs('Fenouil')['Bêta-carotène'] = '12 µg'

# Supression des colonnes en doublon
tablealiments = tablealiments.drop(columns=['Bêta-carotènes', 'Bêta carotènes',
                                            'Bêta carotène'])

# 1 UI de Vitamine A = ~ 2.68 µg. Changement de l'unité de mesure qui était
# en UI
tablealiments.xs('Yuzu')['Vitamine A'] = '243,88 µg'
tablealiments.xs('Acérola')['Vitamine A'] = '2055,56 µg'

# Rename des colonnes en ajoutant leur unité de mesure dans leur nom
for colonne in mg:
    print(colonne)
    tablealiments = tablealiments.rename(
        columns={colonne: str(colonne) + ' mg'})

for colonne in µg:
    print(colonne)
    tablealiments = tablealiments.rename(
        columns={colonne: str(colonne) + ' µg'})

for colonne in g:
    print(colonne)
    tablealiments = tablealiments.rename(
        columns={colonne: str(colonne) + ' g'})

for colonne in kcal:
    print(colonne)
    tablealiments = tablealiments.rename(
        columns={colonne: str(colonne) + ' kcal'})

for colonne in kJ:
    print(colonne)
    tablealiments = tablealiments.rename(
        columns={colonne: str(colonne) + ' kJ'})

# Liste des nouvelles colonnes avec les unités de mesure
liste_colonnes = []
liste_unite_mesure = ['mg', 'g', 'µ', 'kcal', 'kJ']
for nom_colonne in tablealiments.columns:
    liste_colonnes.append(nom_colonne)

# Suppresion des unités de mesure dans la dataframe
for colonne in liste_colonnes:
    for um in liste_unite_mesure:
        tablealiments[colonne] = tablealiments[colonne].replace({um: ''},
                                                                regex=True)

# Toutes les ',' sont remplacées par des '.'
# Et les \xa0 par ''
tablealiments = tablealiments.replace({',': '.'}, regex=True)
tablealiments = tablealiments.replace({'\xa0': ''}, regex=True)

# Suppresion de cette valeur qui était "Donnée non disponible" et d'un 's'
tablealiments['Fibres alimentaires g']['Mangue'] = '1,6'
tablealiments['Calories kcal']['Acérola'] = '32'

# Création du fichier excel
tablealiments.to_excel("tableau_des_aliments6.xlsx")

# Ajout d'une colonne Aliments
tablealiments['Aliments'] = listealiments
tablealiments.to_excel("tableau_des_aliments7.xlsx")

# Création d'excel par aliment
for i, x in zip(tableaux_scrapes, fruits_legumes):
    print(x)
    print(i)
    i.to_excel(str(x) + ".xlsx")

# =============================================================================
#                        REMISE EN FORME DE LA TABLE
# =============================================================================

# Des majuscules étaient présentes dans la table, je réutilise la remise en
# forme que j'avais faite dans le code 'optimisation.py'
con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql1 = "select * from liste_doublons"
df_colonne_minuscule = pd.read_sql_query(sql1, con)
cur = con.cursor()

tablealiments = pd.read_excel('tableau_des_aliments6.xlsx', index_col=0)

for valeurs in tablealiments.columns:
    for aliment in listealiments:
        if math.isnan(tablealiments.loc[aliment][valeurs]) is True:
            tablealiments.loc[aliment][valeurs] = 0

# Remplace les colonnes majuscule en minuscule
liste_colonne_vide = df_colonne_minuscule.columns.tolist()
liste_colonne_vide.remove('level_0')
liste_colonne_vide.remove('index')
liste_colonne_maj = tablealiments.columns.tolist()
# Ajoute la colonne manquante "Nom" en tete de liste
liste_colonne_maj.insert(0, "Nom")

for maj, minu in zip(liste_colonne_maj, liste_colonne_vide):
    print(minu)
    tablealiments = tablealiments.rename(columns={maj: minu})

# Une majuscule n'était pas prise en compte (vitamine_C_mg)
tablealiments = tablealiments.rename(columns={'vitamine_C_mg':
                                                  'vitamine_c_mg'})

# Remplace toutes les lettre majuscules en minuscules.
tablealiments = tablealiments.apply(
    lambda x: x.astype(str).str.lower())

# Remplace toutes les lettre majuscules de l'index en minuscules. 
tablealiments.index = tablealiments.index.str.lower()

# Change les types de colonne texte en flottant, et rename de la vitamine c
liste_colonne_type = liste_colonne_vide
liste_colonne_type.remove('nom')
liste_colonne_type = np.char.replace(liste_colonne_type,
                                     'vitamine_C_mg', 'vitamine_c_mg')
for colonne in liste_colonne_type:
    tablealiments[colonne] = tablealiments[colonne].astype(float)

# Il y a encore deux colonnes identiques "Fibres alimentaires g" et "Fibres g"
# la colonne "Fibres g" sera gardée
for aliment in tablealiments.index:
    if tablealiments.xs(aliment)['fibres_g'] == 0.0:
        valeur = tablealiments['fibres_alimentaires_g'][aliment]
        tablealiments.xs(aliment)['fibres_g'] = valeur
# Suppression de la colonne 'fibres_alimentaires_g'
tablealiments = tablealiments.drop(columns=['fibres_alimentaires_g'])

tablealiments.to_excel("tableau_des_aliments8.xlsx")

engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')
tablealiments.to_sql('aliments2', engine, index=True)

# =============================================================================
#                      REMPLISSAGE DE LA BDD MISE AU NORME
# =============================================================================

tablealiments = pd.read_excel('tableau_des_aliments8.xlsx', index_col=0)

con = psycopg2.connect(host="localhost",
                       database="BDD CERTIF",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql = "select * from aliments;"
tablealiments_postgre = pd.read_sql_query(sql, con)
cur = con.cursor()

# Ces colonnes n'étaient pas en type float
tablealiments['vitamine_k_µg'] = tablealiments['vitamine_k_µg'].astype('float')
tablealiments['calories_kcal'] = tablealiments['calories_kcal'].astype('float')
tablealiments['potassium_mg'] = tablealiments['potassium_mg'].astype('float')

tablealiments.to_excel("tableau_des_aliments9.xlsx")

dictionnaire_table = tablealiments.to_dict("records")

aliments = []

for aliment in tablealiments.index:
    aliments.append(aliment)

for aliment in aliments:
    requete = "INSERT INTO aliments (nom_aliment) VALUES ("
    requete += "'" + str(aliment) + "'" + ")"
    cur.execute(requete)
    con.commit()

for aliment in aliments:
    for dictionnaire in dictionnaire_table:
        requete = "INSERT INTO aliments (eau_g, vitamine_k_mcg, "
        requete += "sodium_mg, cuivre_mg, proteines_g, beta_carotene_mcg, "
        requete += "magnesium_mg, phosphore_mg, calories_kcal, vitamine_C_mg, "
        requete += "potassium_mg, vitamine_b2_mg, vitamine_b1_mg, "
        requete += "manganese_mg, zinc_mg, vitamine_e_mg, vitamine_a_mg, "
        requete += "fibres_g, vitamine_b9_mcg, lipides_g, vitamine_b3_mg, "
        requete += "vitamine_b6_mg, calcium_mg, glucides_g, vitamine_b5_mg, "
        requete += "energie_kj, fer_mg) VALUES ("
        for colonne, valeur in dictionnaire.items():
            requete += "'" + str(valeur) + "'" + ","
        # Supprime la dernière virgule
        requete = requete[:-1]
        requete += ")"
        cur.execute(requete)
        con.commit()
