Flask==1.1.2
numpy==1.18.4
pandas==1.0.4
selenium==3.141.0
SQLAlchemy==1.3.19
xlrd==1.2.0
