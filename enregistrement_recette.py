# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 16:08:39 2020

Permettre à l'utilisateur d'inserer sa recette dans la base de donnée.

@author: Reda
"""

import pandas as pd
import psycopg2


def chargement_recette(recette):
    con = psycopg2.connect(host="localhost",
                           database="ProjetPerso",
                           user="postgres",
                           password="digifab",
                           port=5432)

    sql = "select * from recettes;"

    table_recettes = pd.read_sql_query(sql, con)
    cur = con.cursor()

    character = ""
    for aliment, quantite in recette_utilisateur.items():
        character += str(aliment) + str(quantite) + "g "

    requete = "INSERT INTO recettes (recette) VALUES " + "(" + "'" + str(
        character) + "'" + ")"
    cur.execute(requete)
    con.commit()
    return table_recettes


# Pour intégrer la recette à la dataframe, la colonne recette doit être la
# valeur de tout le dictionnaire.
recette_utilisateur = {'Abricot': 100, 'Avocat': 100, 'Betterave': 100,
                       "Frite": 100, "Pizza": 10
                       }

chargement_recette(recette_utilisateur)
